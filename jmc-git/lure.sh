
name='jmc-git'
version='cc45c74'
release=1
desc='JMC (JavaScript-like Minecraft Function) is a mcfunction extension language for making Minecraft Datapack.'
homepage='https://wingedseal.github.io/jmc'
architectures=('amd64')
license=('MIT')
provides=('JMC')
conflicts=('JMC')

deps_amd64=('python3' 'patchelf')
deps_amd64_debian=('python3' 'patchelf')

sources_amd64=("git+https://github.com/WingedSeal/jmc.git")
checksums=('SKIP')

prepare() {
	cd "${srcdir}/jmc"
}

build() {
	cd "${srcdir}/jmc"
    pip install -r build_requirements.txt
	./build_linux.sh
}

package() {
    mv "${srcdir}/jmc/dist/JMC.bin" "${srcdir}/jmc/dist/jmc" 
    mkdir -p "${pkgdir}/usr/bin/"
	install "${srcdir}/jmc/dist/jmc" "${pkgdir}/usr/bin/jmc"
}
